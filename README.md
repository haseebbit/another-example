## Prerequisites

	*Python 2.7
	*SQLite

Test Deployment
---------------
	$ git clone https://haseebbit@bitbucket.org/haseebbit/another-example.git
	$ cd ~/another-example

Run the server
--------------
	$ python another.py
	
To view the Machines
--------------------
             /machines
             

To add Machine
--------------
         /machines/new
