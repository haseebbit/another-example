from werkzeug import url_decode
from datetime import datetime
from flask import Flask, render_template, request, redirect, url_for, flash
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login,)



time = datetime.now()
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db = SQLAlchemy(app)


class MethodRewriteMiddleware(object):
    """Middleware for HTTP method rewriting.

    Snippet: http://flask.pocoo.org/snippets/38/
    """

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        if 'METHOD_OVERRIDE' in environ.get('QUERY_STRING', ''):
            args = url_decode(environ['QUERY_STRING'])
            method = args.get('__METHOD_OVERRIDE__')
            if method:
                method = method.encode('ascii', 'replace')
                environ['REQUEST_METHOD'] = method
        return self.app(environ, start_response)
        


class User(UserMixin):
    def __init__(self, name, id, active=True):
        self.name = name
        self.id = id
        self.active = active
    
    def is_active(self):
        return self.active
USERS = {
    1: User(u"user1", 1),
    2: User(u"user2", 2),
    3: User(u"user3", 3, False),
}

USER_NAMES = dict((u.name, u) for u in USERS.itervalues())

login_manager = LoginManager()

login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."


class Machine(db.Model):
	"""A fake Model """
	id = db.Column(db.Integer, primary_key=True)
	result = db.Column(db.Integer)
	time = db.Column(db.DateTime)

	def __init__(self, result,id = None, time = None):
		self.id=id
		self.result = result
		if time is None:
			time = datetime.now()
		self.time = time

db.create_all()

app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'secret'
app.wsgi_app = MethodRewriteMiddleware(app.wsgi_app)

@login_manager.user_loader
def load_user(id):
    return USERS.get(int(id))


login_manager.setup_app(app)


@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST" and "username" in request.form:
        username = request.form["username"]
        if username in USER_NAMES:
            remember = request.form.get("remember", "no") == "yes"
            if login_user(USER_NAMES[username], remember=remember):
                flash("Logged in!")
                return redirect(request.args.get("next") or url_for("show_machine.html"))
            else:
                flash("Sorry, but you could not log in.")
        else:
            flash(u"Invalid username.")
    return render_template("login.html")


@app.route('/machines')
def list_machines():
	"""GET /machines Lists all machines"""
	machines = [ Machine(id=1, result=3 ,time=datetime.now()), Machine(id=2, result=54, time=datetime.now()) ] # Your query here ;)
	db.session.add_all(machines)
	db.session.commit()
	machines = Machine.query.all()
	return render_template('list_machines.html', machines=machines)

@app.route('/machines/<id>')
def show_machines(id):
	"""GET /machines/<id>Get a machine by its id"""
	machine = Machine(id=id, result=4, time=datetime.now()) # Your query here ;)
	return render_template('show_machine.html', machine=machine)

@app.route('/machines/new')
def new_machine():
	"""GET /machine/new The form for a new machine"""
	return render_template('new_machine.html')

@app.route('/machines/create/', methods=['GET','POST'])
@login_required
def create_machine():
	"""POST /machines Receives a machine data and saves it"""
	result = request.form['result']
	machine = Machine(result=result, time=datetime.now()) # Save it
	db.session.add(machine)
	db.session.commit()
	flash('Machine %s sucessful saved!' % machine.result)
	return redirect(url_for('show_machine', machine=machine))

@app.route('/machines/<id>', methods=['PUT'])
def update_machine(id):
	"""PUT /machine/<id> Updates a machine"""
	machine = Machine(id=id, result=99,time=datetime.now()) # Your query
	machine.result = request.form['result'] # Save it
	flash('Machine %s updated!' % machine.result)
	db.session.commit()
	return redirect(url_for('show_machine', id=machine.id))


@app.route('/machines/<id>', methods=['DELETE'])
def delete_machine(id):
	"""DELETE /machines/<id> Deletes a machines"""
	machine = Machine(id=id, result=u"My machine to be deleted", time=datetime.now()) # Your query
	db.session.delete(machine)
	db.session.commit()
	flash('Machine %s deleted!' % machine.result)
	return redirect(url_for('list_machines'))
	

@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out.")
    return redirect(url_for("machines"))


if __name__ == '__main__':
	app.run()
